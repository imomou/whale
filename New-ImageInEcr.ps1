param(
    $region = "ap-southeast-2"
)

# Perform login
$token = Get-ECRAuthorizationToken -Region $region
$tokenSegments = [System.Text.Encoding]::ASCII.GetString([System.Convert]::FromBase64String($token.AuthorizationToken)).Split(":")
$hostName = (New-Object System.Uri $token.ProxyEndpoint).DnsSafeHost
docker login -u $($tokenSegments[0]) -p $($tokenSegments[1]) $hostName

# Set repo name
$reponame = (Get-Item .\).Name

# Build
$imagetag = "500107934646.dkr.ecr.$region.amazonaws.com/$($reponame):$env:BUILD_BUILDNUMBER"
docker build -t $imagetag .

# Check if repository exist
$hasRepository = ((Get-ECRRepository) | ? {
    $_.RepositoryName -eq $reponame
}).Count -eq 1
# Create if not.
if (!$hasRepository) {
    New-ECRRepository -RepositoryName $reponame
}

# Push
docker push $imagetag

# Set result
Set-Content -Value $imagetag -Path .\image-tag