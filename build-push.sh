#!/bin/bash

IMAGE_TAG=500107934646.dkr.ecr.ap-southeast-2.amazonaws.com/${PWD##*/}:$BUILD_BUILDNUMBER
docker build -t $IMAGE_TAG .

aws ecr get-login --region ap-southeast-2 | sh
docker push $IMAGE_TAG

#todo: need to create ECR if not exist.

echo $IMAGE_TAG > image-tag